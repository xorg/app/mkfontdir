mkfontdir creates the fonts.dir files needed by the legacy X server
core font system.   The current implementation has been moved into
the mkfontscale repository:

  https://gitlab.freedesktop.org/xorg/app/mkfontscale

Please submit bug reports and requests to merge patches there.

This repository has been archived and is no longer used.
